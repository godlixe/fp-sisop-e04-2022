#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>
#include <syslog.h>

#define MAX 200
#define PORT 8081
#define SA struct sockaddr

char *database = NULL;

char *getColName(char *str)
{

	char holder[7], *ret;
	int hold_counter = 0, allowed = 0;
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i] == ' ')
		{
			holder[hold_counter] = str[i + 1];
			allowed = 1;
			hold_counter++;
			continue;
		}
		if (allowed)
		{
			holder[hold_counter] = str[i + 1];
			hold_counter++;
		}
	}
	ret = holder;
	return ret;
}

char *sanitizeString(char *str)
{
	int i, j, len = strlen(str);
	char *ret;
	for (i = 0; i < len; i++)
	{
		if (str[i] == '(' || str[i] == ')' || str[i] == ',' || str[i] == '\n' || str[i] == '\0')
		{
			for (j = i; j < len; j++)
			{
				str[j] = str[j + 1];
			}
			len--;
			i--;
		}
	}
	ret = str;
	return ret;
}

char *createDatabase(char *cmd, char *resp)
{
	char dirpath[200];
	sprintf(dirpath, "/home/alex/fp-sisop-E04-2022/%s", sanitizeString(cmd));
	int check = mkdir(dirpath, 0777);
	if (!check)
	{
		sprintf(resp, "%s", "Database created");
		return resp;
	}
	else
	{
		sprintf(resp, "%s", "Unable to create database");
		return resp;
	}
}

char *createTable(char *cmd, char *fullCmd, char *resp)
{
	FILE *fp;
	char path[200], *nextCmd, *pFullCmd;
	int status;

	if (database == NULL)
	{
		sprintf(resp, "%s", "No Database Delected");
		return resp;
	}

	nextCmd = strtok(cmd, " ");
	sprintf(path, "/home/alex/fp-sisop-E04-2022/%s/%s", sanitizeString(database), nextCmd);
	fp = fopen(path, "a+");

	if (fp == NULL)
	{
		sprintf(resp, "%s", "Table cannot be created");
		return resp;
	}

	pFullCmd = strtok(fullCmd, "(");
	pFullCmd = strtok(NULL, "(");
	pFullCmd = strtok(pFullCmd, " ");

	int delim_counter = 0;
	while (pFullCmd != NULL)
	{
		fprintf(fp, "%s", sanitizeString(pFullCmd));
		if (delim_counter % 2 == 0)
			fprintf(fp, "%s", " ");
		if (delim_counter != 0 && delim_counter % 2 != 0)
			fprintf(fp, "%s", "; ");
		pFullCmd = strtok(NULL, " ");
		delim_counter++;
	}

	fclose(fp);

	sprintf(resp, "%s", "Table created successfully");
	return resp;
}

char *insert(char *cmd, char *fullCmd, char *resp)
{
	FILE *fp;
	char path[200], col[1000][7], tempCol[1000], *nextCmd, *pFullCmd;
	int status;

	if (database == NULL)
	{
		sprintf(resp, "%s", "No Database Delected");
		return resp;
	}

	nextCmd = strtok(cmd, " ");
	sprintf(path, "/home/alex/fp-sisop-E04-2022/%s/%s", sanitizeString(database), sanitizeString(nextCmd));
	fp = fopen(path, "a+");

	fgets(tempCol, 1000, fp);

	if (fp == NULL)
	{
		sprintf(resp, "%s", "Table cannot be created");
		return resp;
	}

	int col_counter = 0;
	char *pCol1;

	// get into col type;col type
	pCol1 = strtok(tempCol, ";");

	while (pCol1 != NULL)
	{
		sprintf(col[col_counter], "%s", getColName(sanitizeString(pCol1)));
		pCol1 = strtok(NULL, ";");
		col_counter++;
	}

	pFullCmd = strtok(fullCmd, "(");
	pFullCmd = strtok(NULL, "(");
	pFullCmd = strtok(pFullCmd, " ");
	fprintf(fp, "%s", "\n");
	int delim_counter = 0;
	while (pFullCmd != NULL)
	{
		fprintf(fp, "%s;", sanitizeString(pFullCmd));
		pFullCmd = strtok(NULL, " ");
		delim_counter++;
	}

	fclose(fp);

	sprintf(resp, "%s", "Data inserted successfully");
	return resp;
}

void func(int connfd)
{
	char buff[MAX], *pCommand, *pCommandNext, command[100], response[200], fullCmd[200];
	int n;
	// infinite loop for chat
	for (;;)
	{
		bzero(buff, MAX);
		bzero(response, 200);

		read(connfd, buff, sizeof(buff));
		sprintf(fullCmd, "%s", buff);

		pCommand = strtok(buff, " ");
		if (!strcmp(pCommand, "create"))
		{
			pCommand = strtok(NULL, " ");
			if (!strcmp(pCommand, "database"))
			{
				pCommand = strtok(NULL, " ");
				createDatabase(pCommand, response);
			}
			else if (!strcmp(pCommand, "table"))
			{
				pCommand = strtok(NULL, " ");
				createTable(pCommand, fullCmd, response);
			}
			else
			{
				printf("%s", command);
			}
		}
		else if (!strcmp(pCommand, "use"))
		{
			char db[200];
			pCommand = strtok(NULL, " ");
			sprintf(db, "%s", pCommand);
			database = db;
			sprintf(response, "%s %s", "Using database", sanitizeString(db));
		}
		else if (!strcmp(pCommand, "insert"))
		{
			char db[200];
			pCommand = strtok(NULL, " ");
			// Insert table name
			pCommand = strtok(NULL, " ");
			insert(pCommand, fullCmd, response);
		}
		else
		{
			strcpy(response, "Lol");
		}
		bzero(buff, MAX);
		n = 0;
		// copy server message in the buffer
		// while ((buff[n++] = getchar()) != '\n')
		// 	;

		// and send that buffer to client
		write(connfd, response, sizeof(response));

		// if msg contains "Exit" then server exit and chat ended.
		if (strncmp("exit", buff, 4) == 0)
		{
			printf("Server Exit...\n");
			break;
		}
	}
}

int main()
{

	pid_t pid, sid; // Variabel untuk menyimpan PID

	pid = fork(); // Menyimpan PID dari Child Process

	/* Keluar saat fork gagal
	 * (nilai variabel pid < 0) */
	if (pid < 0)
	{
		exit(EXIT_FAILURE);
	}

	/* Keluar saat fork berhasil
	 * (nilai variabel pid adalah PID dari child process) */
	if (pid > 0)
	{
		exit(EXIT_SUCCESS);
	}

	umask(0);

	sid = setsid();
	if (sid < 0)
	{
		exit(EXIT_FAILURE);
	}

	if ((chdir("/")) < 0)
	{
		exit(EXIT_FAILURE);
	}

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	while (1)
	{
		int sockfd, connfd, len;
		struct sockaddr_in servaddr, cli;
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd == -1)
		{
			printf("socket creation failed...\n");
			exit(0);
		}
		else
			printf("Socket successfully created..\n");
		bzero(&servaddr, sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		servaddr.sin_port = htons(PORT);

		if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
		{
			printf("socket bind failed...\n");
			exit(0);
		}
		else
			printf("Socket successfully binded..\n");

		if ((listen(sockfd, 5)) != 0)
		{
			printf("Listen failed...\n");
			exit(0);
		}
		else
			printf("Server listening..\n");
		len = sizeof(cli);

		connfd = accept(sockfd, (SA *)&cli, &len);
		if (connfd < 0)
		{
			printf("server accept failed...\n");
			exit(0);
		}
		else
			printf("server accept the client...\n");

		func(connfd);
		close(sockfd);
	}
}
