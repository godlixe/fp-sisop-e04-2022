#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdbool.h>
#define MAX 200
#define PORT 8081
#define SA struct sockaddr

void func(int sockfd)
{
    char buff[MAX];
    int n;
    for (;;)
    {
        bzero(buff, sizeof(buff));
        printf("mysql> ");
        n = 0;
        while ((buff[n++] = getchar()) != '\n')
            ;
        write(sockfd, buff, sizeof(buff));
        bzero(buff, sizeof(buff));
        read(sockfd, buff, sizeof(buff));
        printf("From Server : %s\n", buff);
        if ((strncmp(buff, "exit", 4)) == 0)
        {
            printf("Client Exit...\n");
            break;
        }
    }
}

int checkAuth(int argc, char *argv[])
{
    FILE *fp;
    const int bufferLength = 2048;
    char buffer[bufferLength], tempUnamePass[bufferLength];
    sprintf(tempUnamePass, "%s:%s", argv[2], argv[4]);
    fp = fopen("users.txt", "a+");

    while (fgets(buffer, bufferLength, fp))
    {
        buffer[strlen(buffer) - 1] = '\0';
        if (!strcmp(tempUnamePass, buffer))
        {
            fclose(fp);
            return 1;
        }
    }
    return 0;
    fclose(fp);
}

int main(int argc, char *argv[])
{
    if (checkAuth(argc, argv))
    {
        printf("Logged in!\n");
    }
    else
    {
        printf("Not logged in\n");
        exit(1);
    }
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    func(sockfd);

    close(sockfd);
}
